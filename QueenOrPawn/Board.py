import Colors

class Board:
    def __init__(self, can):
        self.field = [[0 for i in range(8)] for j in range(8)]
        self.can = can

    def insert(self, position, chessman):
        self.field[position[0]][position[1]] = chessman
        self.can.create_image(75 + 50 * position[0], 75 + 50 * position[1], image=chessman.imgobj)

    def erase(self, position):
        self.field[position[0]][position[1]] = 0
        self.draw_cell(50 + 50 * position[0], 50 + 50 * position[1], Colors.board_colors[(position[0] + position[1] + 1) % 2])

    def cleane(self):
        self.field = [[0 for i in range(8)] for j in range(8)]
        Board.draw()

    def show(self):
        for i in range(8):
            for j in range(8):
                if j != 7:
                    print(self.field[i][j], end=" ")
                else:
                    print(self.field[i][j])

    def draw_cell(self, x, y, color):
        self.can.create_rectangle(x, y, x + 50, y + 50, fill=color)

    def draw(self):
        for v in range(8):
            for h in range(8):
                self.draw_cell(50 * h + 50, 50 * v + 50, Colors.board_colors[(v + h + 1) % 2])

